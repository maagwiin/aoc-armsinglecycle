onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /testbench/clk
add wave -noupdate -radix hexadecimal /testbench/reset
add wave -noupdate -radix hexadecimal /testbench/dut/arm/PC
add wave -noupdate -radix hexadecimal /testbench/dut/arm/Instr
add wave -noupdate /testbench/dut/arm/dp/SrcA
add wave -noupdate /testbench/dut/arm/dp/SrcB
add wave -noupdate /testbench/dut/arm/c/dec/Branch
add wave -noupdate /testbench/dut/arm/dp/AluResult
add wave -noupdate /testbench/dut/arm/c/cl/Flags
add wave -noupdate /testbench/dut/arm/c/cl/CondEx
add wave -noupdate /testbench/dut/arm/dp/WriteData
add wave -noupdate /testbench/dut/arm/c/MemWrite
add wave -noupdate /testbench/dut/arm/dp/ReadData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 235
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {144 ps}
